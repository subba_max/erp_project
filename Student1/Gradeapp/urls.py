from django.urls import path
from . import views
# admin.site.site_header ="devloper"
urlpatterns = [
    path('',views.Home),
    path('Teacher',views.Teacher,name='Teacher'),
    path('Login',views.Login,name='Login')
]
