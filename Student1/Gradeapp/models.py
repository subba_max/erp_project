from django.db import models

# Create your models here.
class TeacherLogin(models.Model):
    username =models.CharField(max_length=50)
    password=  models.CharField(max_length = 20)
    


class Grade(models.Model):
    Grade = models.CharField(primary_key=True, max_length=50)
    # section = models.ForeignKey("sections", on_delete=models.CASCADE,default='')

    def __str__(self):
        return self.Grade

class Sections(models.Model):
    Grade = models.ForeignKey('Grade', on_delete=models.CASCADE)
    section = models.CharField( max_length=50, default='enter')
    # Grade = models.ManyToManyField('Grade')
    # section = models.CharField(default='something' ,max_length=50)

    def __str__(self):
        return self.section


class Subjects(models.Model):
    Class =models.ForeignKey("Grade", on_delete=models.CASCADE)
    # sections = models.ForeignKey("sections", on_delete=models.CASCADE)
    subject = models.CharField( max_length=50,default='some')

    def __str__(self):
        return self.subject
    

class SignUp(models.Model):
    enrollmentNumber= models.IntegerField(default=1)
    # sid = models.IntegerField(default=1)
    firstname= models.CharField(max_length=50)
    lastname = models.CharField( max_length=50)
    username = models.CharField( max_length=50)
    password = models.CharField( max_length=50)
    email = models.EmailField(max_length=254)
    Dob = models.DateField(auto_now=False, auto_now_add=False)
    Grade = models.ForeignKey("Grade", on_delete=models.CASCADE)
    section = models.ForeignKey("Sections", on_delete=models.CASCADE,default='select')
    phonenumber = models.CharField(max_length=50)

