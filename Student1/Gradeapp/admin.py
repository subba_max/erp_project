from django.contrib import admin
from .models import Grade, Sections, Subjects,TeacherLogin
# Register your models here.


class Teacheradmin(admin.ModelAdmin):
    list_display= ['username','password']

class Gradeadmin(admin.ModelAdmin):
    list_display = ['Grade']


class Sectionadmin(admin.ModelAdmin):
    list_display =['Grade','section']


class Subjectadmin(admin.ModelAdmin):
    list_display = ['Class','subject']



admin.site.register(Grade,Gradeadmin)
admin.site.register(Sections,Sectionadmin)
admin.site.register(Subjects,Subjectadmin)
admin.site.register(TeacherLogin,Teacheradmin)
 


