from django.contrib import admin
from .models import Grade, Sections, Subjects
# Register your models here.
class Gradeadmin(admin.ModelAdmin):
    list_display=['Grade']

class Sectionadmin(admin.ModelAdmin):
    list_display= ['Grade','section']



admin.site.register(Grade,Gradeadmin)
admin.site.register(Sections,Sectionadmin)
admin.site.register(Subjects)