from django.db import models

# Create your models here.

class Grade(models.Model):
    Grade = models.CharField(primary_key=True, max_length=50)

    def __str__(self):
        return self.Grade


class Sections(models.Model):
    Grade = models.ForeignKey('Grade', on_delete=models.CASCADE)
    section = models.CharField( max_length=50, default='string')

    def __str__(self):
        return self.section




class Subjects(models.Model):
    Class =models.ForeignKey("Grade", on_delete=models.CASCADE)
    sections = models.ForeignKey("sections", on_delete=models.CASCADE)
    subject = models.CharField( max_length=50,default='some')

    def __str__(self):
        return self.subject
    
